import m from 'moment';
import 'moment/locale/ru';
import { date as dateFormat, dateTime as dateTimeFormat } from './config';
import { Convert as convert } from './convert';
import { map, isPlainObject } from 'lodash';

export const formatDateTime = (dateTime, format = dateTimeFormat) => {
  if (!dateTime) return '';
  return m(dateTime).format(format);
};

export const formatDate = (date, format = dateFormat) => {
  if (!date) return '';
  return m(date).format(format);
};

export const cutText = (text, length) => {
  if (!text) return '';
  return (text.length > length) ? `${text.slice(0, length)}...` : text;
};


export const timeAgo = date => m(date).fromNow();

export const cutAddress = (data) => {
  if (!data) return '';
  return data.substr(data.indexOf(',') + 1);
};


const Utils = {
  cutText,
  formatDateTime,
  formatDate,
  convert,
  cutAddress
};

export default Utils;
