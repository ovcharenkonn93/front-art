export const Convert = {

  obj2json(obj) {
    try {
      return JSON.stringify(obj);
    } catch (e) {
      return '{}';
    }
  },

  json2obj(json) {
    try {
      return JSON.parse(json);
    } catch (e) {
      return {};
    }
  },

  obj2params(obj, isQueryString) {
    const prependSign = (isQueryString) ? '?' : '';
    return prependSign +
            Object.keys(obj).map((key) => {
              if (Object.prototype.toString.call(obj[key]) == '[object Array]') {
                let param = '';
                for (let i = 0; i < obj[key].length; i++) {
                  if (Object.prototype.toString.call(obj[key][i]) == '[object Object]') {
                    for (const slk in obj[key][i]) {
                      param += `${encodeURIComponent(key)}[][${slk}]=${encodeURIComponent(obj[key][i][slk])}&`;
                    }
                  } else {
                    param += `${encodeURIComponent(key)}[]=${encodeURIComponent(obj[key][i])}&`;
                  }
                }
                return param.slice(0, -1);
              } else {
                return `${encodeURIComponent(key)}=${
                    encodeURIComponent(obj[key])}`;
              }
            }).join('&');
  },
};
