/**
 *  Prevent multiple api requests with the same parameters.
 *  Use it as decorator.
 */

export const deduplicate = function (target, name, descriptor) {
  const func = descriptor.value;
  const cache = {};

  descriptor.value = function () {
    const args = Array.prototype.slice.call(arguments);
    const key = `${name} ${JSON.stringify(args)}`;

    if (key in cache) return cache[key];

    const result = func.apply(this, arguments);
    const drop = () => {
      delete cache[key];
    };

    result.then(drop, drop);
    cache[key] = result;

    return result;
  };
};
