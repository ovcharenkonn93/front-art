import 'babel-polyfill';
import React, { Component } from 'react';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import configureStore from './stores/configureStores';
import makeRoutes from './routes';

const state = {};

const store = configureStore(state);
const history = syncHistoryWithStore(browserHistory, store);
const routes = makeRoutes(store);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history} routes={routes} />
      </Provider>
    );
  }
}

export default App;
