import 'whatwg-fetch';
// import AppConfig from 'config';
import { isObject, isNull, isNil, isPlainObject } from 'lodash';
import axios from 'axios';
import Utils from '../utils/utils';
import BrowserStorage from '../BrowserStorage';

const parseResponse = (response) => {
  const status = response.status;
  if (status === 204) {
    return ({ data: {} });
  } else if (status >= 200 && status < 300) {
    return response.data;
  }
  return null;
};

const checkToken = (response) => {
  if (response && response.key) {
    BrowserStorage.set('token', response.key);
  }
  return response;
};

const callApi = (endpoint, method, params = null, hasToken = true) => {
  const token = BrowserStorage.get('token') || null;

  // let fullUrl = `https://art-parom.ru/${endpoint}`;
  let fullUrl = `http://localhost:8000/${endpoint}`;

  const query = {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    xsrfHeaderName: 'X-CSRFToken'
  };

  if (!isNull(token) && hasToken) {
    query.headers.Authorization = `Token ${token}`;
  }

  if (params !== null) {
    switch (method) {
      case 'GET': {
        const urlQuery = Object.keys(params)
          .filter(k => !isNil(params[k]))
          .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
          .join('&');
        fullUrl += `?${urlQuery}`;
        break;
      }
      default:
        query.body = Utils.convert.obj2params(params);
    }
  }
  return axios({
    url: fullUrl,
    headers: query.headers,
    withCredentials: false,
    method,
    data: query.body
  }).then(
    parseResponse
    ).then(
    checkToken
    ).catch((data) => {
      if (data.response) {
        if (data.response.status === 401 && BrowserStorage.get('token')) {
          BrowserStorage.remove('token');
          window.location = '/';
          return;
        }
      }
      const error = new Error();
      error.message = data;
      throw error;
    });

};
export const postApi = (endpoint, params, hasToken) => callApi(endpoint, 'POST', params, hasToken);
export const patchApi = (endpoint, params, hasToken) => callApi(endpoint, 'PATCH', params, hasToken);
export const putApi = (endpoint, params, hasToken) => callApi(endpoint, 'PUT', params, hasToken);
export const getApi = (endpoint, params, hasToken) => callApi(endpoint, 'GET', params, hasToken);
export const deleteApi = (endpoint, params, hasToken) => callApi(endpoint, 'DELETE', params, hasToken);
