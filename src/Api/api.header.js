import { getApi, postApi } from './api.core';

export const getPassword = phone => getApi(`api/v0/password/by_phone/${phone}`);
export const auth = (phone, password) => postApi('api/v0/rest-auth/login/',
  {
    username: phone.slice(1),
    password
  });
