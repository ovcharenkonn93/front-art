import { getApi, postApi } from './api.core';

export const getRouteFrom = () => getApi('api/v0/from/');
export const getTo = item => getApi(`api/v0/from/${item}`);
export const getDates = (from, to) => getApi(`api/v0/route/from/${from}/to/${to}/`);
export const getDatesDouble = (from, to) => getApi(`api/v0/route/from/${from}/to/${to}/double/`);
export const registerAuto = (autoMarka, autoModel) => postApi('api/v0/register_auto/', { auto_model: autoModel, auto_marka: autoMarka });
export const getCarOrBuses = (car, buses, auto_marka) => postApi('api/v0/get_car_or_buses/', { car, buses, auto_marka });
export const getModels = () => getApi('api/v0/get_models');
export const getPrice = (auto_model, point_from, point_to, double) => postApi('api/v0/get_price', { auto_model, point_from, point_to, double });
export const getBuyId = ({ dateTo, dateFrom, auto_model, auto_number, first_name, document, email, user, amount, passengers }) => postApi('api/v0/get_id_of_yandex', { dateTo, dateFrom, auto_model, auto_number, first_name, document, email, user, amount, passengers });

