import * as core from './api.core';
import * as header from './api.header';
import * as news from './api.news';
import * as selectFromToList from './api.selectFromToList';

export default {
  core,
  header,
  news,
  selectFromToList
};
