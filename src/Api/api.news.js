import { getApi } from './api.core';

export const getNews = () => getApi('api/v0/news/');
