import { fork } from 'redux-saga/effects';
// import watchSearchMedia from './watchers';
import header from '../components/Header/Header.saga';
import news from '../containers/News/News.saga';
import selectFromToList from '../containers/SelectFromToList/SelectFromToList.saga';

function* startupSaga() {
}

function startRestSagas(...sagas) {
  return sagas.map(saga => fork(saga));
}

export default function* rootSaga() {
  yield fork(startupSaga);

  yield [startRestSagas(
    header,
    news,
    selectFromToList
  )];
}
