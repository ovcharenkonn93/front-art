import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class SelectTo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      selected: null
    };
  }


  onItemClick = (index) => {
    if (this.state.visible) {
      this.setState({ selected: index, visible: false });
      this.props.onSelectTo(index);
    }

  }

  openSelectList = () => {
    if (this.props.active) {
      this.setState({ visible: !this.state.visible });
    }
  }

/* singleItem = () =>{
  <ul className={'from_to-list' + (this.visible ? ' opened' : '')} onClick={this.openSelectList()}>
            <li className="default select-from">
              <p className="list-header">Выберите</p>
              <p className="list-small-text">пункт<br /> назначения</p>
            </li>
} */

  render() {

    const { data } = this.props;
    if (!data) {
      return null;
    }
    const showStatus = this.state.visible ? 'from_to-list opened' : 'from_to-list';
    const disabledBlock = this.props.active ? 'block-main_search-form-from_to' : 'block-main_search-form-from_to disabled_block';


    return (


      <div className="block-main_search-form-from_to-block">
        <div className={disabledBlock}>
          <ul className={showStatus} onClick={() => this.openSelectList()}>
            <li className="default select-to">
              <p className="list-header">Выберите</p>
              <p className="list-small-text">пункт<br /> назначения</p>
            </li>
            { data.map((item, index) =>
              <li key={index} onClick={() => this.onItemClick(item.id)} className={(this.state.selected === item.id) && 'selected select-to'}>
                <p className="list-header">{item.parom_name_to}</p>
                <p className="list-small-text">пункт<br /> назначения</p>
              </li>
            )}
          </ul>
        </div>
      </div>

    );

  }
}


SelectTo.propTypes = { data: PropTypes.array, active: PropTypes.bool };

export default SelectTo ;
