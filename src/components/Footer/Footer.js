import React, { Component } from 'react';
import { Link } from 'react-router';

class Footer extends Component {
  static methodAreOk() {
    return true;
  }
  render() {
    return (
      <footer className="agan-footer-block mdl-mega-footer">
        <div className="mdl-mega-footer--top-section">
          <div className="block-news__waves">
            <img src="/static/images/footer_waves.png" alt="" />
          </div>
        </div>
        <div className="agan-footer mdl-mega-footer--middle-section">
          <div className="mdl-mega-footer--left-section left-side">

            <div className="footer-logo">
              <Link to="/">
                <img src="/static/images/footer_logo.png" alt="" />
                <p>АГАНРЕЧТРАНС</p>
              </Link>
            </div>

            <p className="footer-about_text">Наша компания создана, чтобы помогать людям и бизнесу работать, жить и развиваться.</p>

            <p className="footer-about_text">Наши суда всегда находятся в отличном техническом состоянии, а команда всегдаподготовлена, обучена и готова к работе.</p>
          </div>

          <div className="mdl-mega-footer--right-section right-side">
            <p className="footer-contact_title">Наши контакты :</p>
            <div className="contact">
              <div className="icon-img map-marker contact-icon" />
              <p className="footer-about_text contact-text"> 628614, РФ, Тюменская область, ХМАО - Югра, г. Нижневартовск Юго-Западный промышленный узел, панель 25, ул. 2П-2, д. 97 </p>
            </div>
            <div className="contact">
              <div className="icon-img envelope contact-icon" />
              <p className="footer-about_text contact-text">sk.art2011@yandex.ru</p>
            </div>
            <div className="contact">
              <div className="icon-img telephone contact-icon" />
              <p className="footer-about_text contact-text">Телефон: 8(3466)25-14-14, 8(3466)63-47-42 </p>
            </div>

          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
