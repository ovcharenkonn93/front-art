import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class SelectDirection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      directions: false
    };
  }


  chgArrowDirection = () => {
    this.setState({ directions: !this.state.directions });
    this.props.onSwichDirections(this.state.directions);
  }


  arrowsCombinations = () => {
    if (this.props.data) {
      return (
        <div className="block-main_search-form-arrows">
          <div className="block-main_search-form-arrow_right">
            <img src="/static/images/148233495_back-alt.png" />
          </div>
          <div className="block-main_search-form-arrow_left" />
        </div>
      );
    }
    return (
      <div className="block-main_search-form-arrows">
        <div className="block-main_search-form-arrow_right">
          <img src="/static/images/148233495_back-alt.png" />
        </div>
        <div className="block-main_search-form-arrow_left">
          <img src="/static/images/1482334956_back-alt.png" />
        </div>
      </div>
    );
  }


  render() {

    const { data } = this.props;

    return (

      <div className="block-main_search-form-arrows-block" onClick={() => this.chgArrowDirection()} >
        {this.arrowsCombinations() }

      </div>


    );

  }
}


SelectDirection.propTypes = { directions: PropTypes.bool };

export default SelectDirection;
