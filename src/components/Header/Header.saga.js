import { put, call, fork } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import api from '../../Api';
import * as actions from './Header.actionTypes';
import { serverError } from '../../containers/Layout/Layout.actions';


function* getPassword() {
  yield* takeEvery(actions.HEADER_GET_PASSWORD, function* load(action) {
    try {
      const data = yield call(api.header.getPassword, action.phone);
      yield put({ type: actions.HEADER_GET_PASSWORD_COMPLETE, ...data });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.HEADER_GET_PASSWORD_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

function* auth() {
  yield* takeEvery(actions.HEADER_AUTH, function* load(action) {
    try {
      yield call(api.header.auth, action.phone, action.password);
      yield put({ type: actions.HEADER_AUTH_COMPLETE });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.HEADER_AUTH_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

export default function* watch() {
  yield [
    fork(getPassword),
    fork(auth)
  ];
}
