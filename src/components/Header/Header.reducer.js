import * as actions from './Header.actionTypes';

const initialState = {
  loading: false,
  send: false,
  phone: null,
  visible: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.HEADER_INIT:
      return {
        ...state
      };
    case actions.HEADER_GET_PASSWORD:
      return {
        ...state,
        loading: true,
        send: true,
        phone: action.phone
      };
    case actions.HEADER_RESEND:
      return {
        ...state,
        send: false,
        message: false
      };
    case actions.HEADER_GET_PASSWORD_COMPLETE:
      return {
        ...state,
        loading: false
      };
    case actions.HEADER_GET_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        message: action.error.message.response.data.detail
      };
    case actions.HEADER_AUTH_COMPLETE:
      return {
        ...state,
        loading: false,
        message2: null,
        visible: false
      };
    case actions.HEADER_AUTH:
      return {
        ...state,
        loading: true,
        message2: null

      };
    case actions.HEADER_AUTH_ERROR:
      return {
        ...state,
        loading: false,
        message2: 'Авторизируйтес'
      };
    case actions.HEADER_VISIBLE_TOGLE:
      return {
        ...state,
        visible: !state.visible
      };
    default:
      return {
        ...state
      };
  }
};
