import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import InputElement from 'react-input-mask';
import * as actions from './Header.actionTypes';
import BrowserStorage from '../../BrowserStorage';

const mapStateToProps = state => ({
  ...state.header
});

@connect(mapStateToProps)
class Header extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.dispatch({
      type: actions.HEADER_INIT
    });
  }


  getPassword = (e) => {
    e.stopPropagation();
    this.props.dispatch({
      type: actions.HEADER_GET_PASSWORD,
      phone: this.phone.value
    });
  }

  sendAuth = (e) => {
    e.stopPropagation();
    // Lockr.set('token', this.phone.value);
    this.props.dispatch({
      type: actions.HEADER_AUTH,
      phone: this.props.phone,
      password: this.password.value
    });
  }

  authFormClick = () => {
    if (BrowserStorage.get('token')) {
      browserHistory.push('/personal');
      return;
    }
    this.props.dispatch({ type: actions.HEADER_VISIBLE_TOGLE });
  }

  retryAuth = () => {
    this.props.dispatch({ type: actions.HEADER_RESEND });
  }

  renderMenu = () => (
    <div className="authorization-form" id="authorization-form">
      <p className="modal-header">Авторизация</p>

      {this.props.message ?
        <div>
          <p onClick={this.retryAuth} style={{ color: 'red', borderBottom: '1px dashed red', cursor: 'pointer', display: 'inline-block' }}>{this.props.message}</p>
        </div>
        :
        (!this.props.send ?
          <div>
            <div className="authorization-form-hint">
              <p>Введите Ваш телефон для получения временного пароля</p>
            </div>
            <p>
              <InputElement type="text" alwaysShowMask ref={phone => this.phone = phone} mask="+79999999999" />
            </p>
            <p><input type="submit" onClick={this.getPassword} value="" /></p>
          </div> :
          <div>
            <div className="authorization-form-hint">
              <p>Сейчас на Ваш телефон придет сообщение с временным паролем</p>
            </div>
            {this.props.loading ?
              <div>
                <p style={{ color: 'red' }}>{this.props.message ? this.props.message : 'Сообщение отправляется'}</p>
              </div> :
              <div>
                <p><InputElement type="text" ref={password => this.password = password} mask="9999" /></p>
                {this.props.message2 &&
                  <p style={{ color: 'red', borderBottom: '1px dashed red', display: 'inline-block' }}>{this.props.message2}</p>}
                <p><input type="submit" onClick={this.sendAuth} value="" /></p>

              </div>
          }
          </div>
        )
      }
    </div>
  );

  render() {
    return (
      <div className="first-page">
        <div className="block-header">
          <div className="block-header-left">
            <Link to="/">
              <div className="block-header-logo">
                <img src="/static/images/logo.png" alt="logo" /><p><i>АГАН</i>РЕЧТРАНС</p>
              </div>
            </Link>
          </div>
          <div className="block-header-right">
            <div className="block-header-person">
              <div className="block-header-person-button" id="block-header-person-button" onClick={this.authFormClick}>
                <p>Личный кабинет</p>
                <img src="/static//images/avatar.png" />
              </div>
              {this.props.visible && this.renderMenu()}
            </div>
          </div>
        </div>

      </div>
    );
  }
}

Header.propTypes = {
};

export default Header;
