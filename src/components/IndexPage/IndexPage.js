import React, { Component } from 'react';
import News from '../../containers/News/News';
import SelectFromToList from '../../containers/SelectFromToList/SelectFromToList';


class IndexPage extends Component {
  static methodAreOk() {
    return true;
  }

  render() {
    return (
      <div>
        <div className="select-from_to-list">
          <SelectFromToList />
          <div className="block-main_search-wave">
            <img src="/static/images/waves.png" />
          </div>
          <div className="block-main_search-bottom" />
        </div>
        <News />
      </div>
    );
  }
}

export default IndexPage;
