import * as actions from './IndexPage.actionTypes';

const initialState = {

};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.INDEX_PAGE_INIT:
      return {
        ...state
      };
    case actions.INDEX_PAGE_INITIATED:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_TO:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_TO_COMPLETE:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_DATE:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_DATE_COMPLETE:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_DATE_BACK:
      return {
        ...state

      };
    case actions.INDEX_PAGE_GET_PAROM_DATE_BACK_COMPLETE:
      return {
        ...state

      };
    case actions.INDEX_PAGE_ERROR:
      return {
        ...state
      };
    default:
      return {
        ...state
      };
  }
};
