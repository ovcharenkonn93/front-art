import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';


class SelectFrom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      selected: null
    };
  }


  onItemClick = (id) => {
    if (this.state.visible) {
      this.setState({ selected: id, visible: false });
      this.props.onSelectFrom(id);
    }


  }

  openSelectList = () => {
    this.setState({ visible: !this.state.visible });
  }

/* singleItem = () =>{
  <ul className={'from_to-list' + (this.visible ? ' opened' : '')} onClick={this.openSelectList()}>
            <li className="default select-from">
              <p className="list-header">Выберите</p>
              <p className="list-small-text">пункт<br /> назначения</p>
            </li>
} */

  render() {

    const { data } = this.props;
    if (!data) {
      return null;
    }
    const showStatus = this.state.visible ? 'from_to-list opened' : 'from_to-list';

    const disabledPort = data ? 'selected select-from disabled_from_to' : 'selected select-from';

    return (


      <div className="block-main_search-form-from_to-block">
        <div className="block-main_search-form-from_to">
          <ul className={showStatus} onClick={() => this.openSelectList()}>
            <li className="default select-from">
              <p className="list-header">Выберите</p>
              <p className="list-small-text">пункт<br /> отправления </p>
            </li>
            { data.map((item, index) =>
              <li key={index} onClick={() => this.onItemClick(item.id)} className={(this.state.selected === item.id) && 'selected select-from'}>
                <p className="list-header">{item.parom_name_from}</p>
                <p className="list-small-text">пункт<br /> отправления</p>
              </li>
            )}
          </ul>
        </div>
      </div>

    );

  }
}


SelectFrom.propTypes = { data: PropTypes.array };

export default SelectFrom ;
