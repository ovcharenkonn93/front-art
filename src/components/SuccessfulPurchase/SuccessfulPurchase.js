import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import ErrorSelector from '../../components/ErrorSelector/ErrorSelector';

class SuccessfulPurchase extends Component {
  static methodAreOk() {
    return true;
  }
  closeModal=() => {
    console.log('kjhhhj');
  }
  render() {
    return (
      <div className="block-successful-page">
        <div className="text-container">
          <Link to="/" className="successful-page-header-text">
            Покупка билета завершена успешно!
          </Link>
          <ErrorSelector data="Hello" open closeModal={this.closeModal()} />
        </div>
      </div>
    );
  }
}
export default SuccessfulPurchase;
