import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';


class RejectPurchase extends Component {
  static methodAreOk() {
    return true;
  }

  render() {
    return (
      <div className="block-successful-page">
        <div className="text-container">
          <Link to="/" className="reject-page-header-text"> Покупка билета неуспешна!</Link>
        </div>
      </div>
    );
  }
}
export default RejectPurchase ;
