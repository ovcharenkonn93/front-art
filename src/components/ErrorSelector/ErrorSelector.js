import React, { Component, PropTypes } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router';
import { connect } from 'react-redux';


const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    right: '0px',
    bottom: '0px',
    border: 'none',
    background: 'transparent',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '0px',
    outline: 'none',
    padding: '0px'

  }

};
class ErrorSelector extends Component {

  render() {
    return (
      <div className="block-successful-page">
        <Modal
          isOpen={this.props.open}
          onRequestClose={this.props.closeModal}
          contentLabel=""
          style={style}
        >
          <div className="overlay error">
            <div className="block-modal error">
              { /* <a href="#close" onClick={this.closeModal} className="close-modal" /> */ }
              <span onClick={this.props.closeModal} className="close-modal" />
              <p className="modal-header">Ошибка</p>
              <div className="modal-form">
                <div className="modal-info-block">
                  <p className="modal-info-text">{this.props.data}</p>
                </div>
              </div>
            </div>
          </div>
        </Modal>


      </div>
    );
  }
}
ErrorSelector.propTypes = { data: PropTypes.string };
export default ErrorSelector;
