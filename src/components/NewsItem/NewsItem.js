import React, { Component, PropTypes } from 'react';


class NewsItem extends Component {
  static componentAreOk() {
    return true;
  }

  NewsClick = () => {
    const { data: { newsData, newsTitle, newsText, newsFullText } } = this.props;

    this.props.onNewsItemClick(newsData, newsTitle, newsText, newsFullText);

  }

  render() {
    const { data: { content, title, date } } = this.props;
    return (
      <div className="block-news__element" onClick={this.NewsClick}>
        <div className="news-element_header ">
          <span className="news-header__text">{date}<i className="icon-img calendar" /></span>
        </div>
        <div className="news-element_body">
          <p className="news-element__title">{title}</p>
          <div className="news-element__text" dangerouslySetInnerHTML={{ __html: content }} />
        </div>
      </div>
    );

  }
}

NewsItem.propTypes = { data: PropTypes.shape({
  content: PropTypes.string,
  date: PropTypes.string,
  title: PropTypes.string })
};

export default NewsItem;
