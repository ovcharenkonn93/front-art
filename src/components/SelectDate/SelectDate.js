import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { includes, isEmpty } from 'lodash';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';

const mapStateToProps = state => ({
  ...state.selectDate
});

@connect(mapStateToProps)
class SelectDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateFrom: null,
      focusedTo: false,
      dateTo: null,
      focusedFrom: false

    };
  }

  onDateChangeFrom = (dateFrom) => {
    this.props.onSelectStartDate(dateFrom);
  }

  onFocusChangeFrom =({ focused }) => {
    this.setState({ focusedFrom: focused });
  }

  onDateChangeTo = (dateTo) => {
    this.props.onSelectArrivalDate(dateTo);
  }

  onFocusChangeTo =({ focused }) => {
    this.setState({ focusedTo: focused });
  }


  render() {

    const { data, datesFrom, datesTo, dateFrom, dateTo } = this.props;
    const disabledCalendar = isEmpty(datesTo) ? 'block-main_search-form-date-from disabled_calendar' : 'block-main_search-form-date-from';
    const disabledBlock = isEmpty(datesFrom) ? 'block-main_search-form-date-to  disabled_calendar' : 'block-main_search-form-date-to ';
    const fromDate = [];
    const fromDateText = [];
    const toDate = [];
    const toDateText = [];
    if (!isEmpty(datesTo)) {
      datesTo.map((item) => {
        toDate.push(moment(item.parom_date, 'YYYY-MM-DD'));
        toDateText.push(item.parom_date);
      });

    }
    if (!isEmpty(datesFrom)) {
      datesFrom.map((item) => {
        fromDate.push(moment(item.parom_date, 'YYYY-MM-DD'));
        fromDateText.push(item.parom_date);
      });
    }
    return (


      <div className="block-main_search-form-date-block">
        <div className="block-main_search-form-date">
          <div className={disabledBlock}>
            <div className="block-main_search-form-date-to-select">
              { /* <p>Дата отправления :</p> <Calendar /> */ }
              <SingleDatePicker
                id="date_input"
                disabled={isEmpty(datesFrom)}
                date={dateFrom}
                isDayBlocked={day => !includes(fromDateText, day.format('YYYY-MM-DD'))}
                focused={this.state.focusedFrom}
                onDateChange={this.onDateChangeFrom}
                onFocusChange={this.onFocusChangeFrom}
                numberOfMonths={1}
                placeholder="Дата отправления: "
              />

            </div>
            <div className="block-main_search-form-date-to-img">
              <img src="/static/images/1482336581_calendar.png" />
            </div>
          </div>
          <div className={disabledCalendar}>
            <div className="block-main_search-form-date-to-select">

              <SingleDatePicker
                id="date_input1"
                disabled={isEmpty(datesTo)}
                date={dateTo}
                isDayBlocked={day => !includes(toDateText, day.format('YYYY-MM-DD'))}
                focused={this.state.focusedTo}
                onDateChange={this.onDateChangeTo}
                onFocusChange={this.onFocusChangeTo}
                numberOfMonths={1}
                placeholder="Дата прибытия: "
              />
            </div>
            <div className="block-main_search-form-date-to-img">
              <img src="/static/images/1482336581_calendar.png" />
            </div>
          </div>
        </div>
      </div>
    );

  }
}


SelectDate.propTypes = { directions: PropTypes.bool, active: PropTypes.bool };

export default SelectDate ;
