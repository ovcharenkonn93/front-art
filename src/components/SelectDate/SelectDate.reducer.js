import * as actions from './SelectDate.actionTypes';


const initialState = {
  selected: false,
  startDate: null,
  arrivalDate: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SELECT_DATE_START_DATE:
      return {
        ...state,
        startDate: action.startDate
      };
    case actions.SELECT_DATE_ARRIVAL_DATE:
      return {
        ...state,
        arrivalDate: action.arrivalDate
      };
    default:
      return {
        ...state
      };
  }
};
