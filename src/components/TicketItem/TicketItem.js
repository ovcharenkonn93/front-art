import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

class TicketItem extends Component {
  static methodAreOk() {
    return true;
  }
  render() {
    const { data: { TicketID, TicketWay, TicketDate } } = this.props;
    return (
      <tr>
        <td className="border-right"><p>{TicketID}</p></td><td className="border-right"><p>{TicketWay}</p></td><td><p>{TicketDate}</p></td>
        <td className="border-left">
          <div className="block-person-buttons">
            <ul>
              <li title="Печать" ><img src="/images/1483052167_vector_66_15.png" alt="" /></li>
              <li title="Вернуть" ><img src="/images/1482881048_arrow-back.png" alt="" /></li>
              <Link to="/personal/ticket/3457645" > <li title="Просмотр" ><img src="/images/1483049305_eye.png" alt="" /></li> </Link>
            </ul>
          </div>
        </td>
      </tr>
    );
  }
}

TicketItem.propTypes = { data: PropTypes.shape({
  TicketID: PropTypes.string.isRequired,
  TicketWay: PropTypes.string.isRequired,
  TicketDate: PropTypes.string.isRequired })
};

export default TicketItem;
