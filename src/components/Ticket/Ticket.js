import React, { Component, PropTypes } from 'react';

const TicketData =
  {
    TicketID: '3457',
    TicketWay: 'Приобье-Салехард,Салехард-Приобье',
    TicketDate: '16.12.16',
    FullName: 'Иванов Иван Петрович',
    Passengers: '1',
    Email: '1@mail.ru',
    Passport: '0000',
    Passenger1_name: 'Сидоров Василий Петрович',
    Passenger1_passport: '0000',
    Passenger2_name: 'Сидоров Василий Петрович',
    Passenger2_passport: '0000',
    Transport_type: 1,
    Transport_mark: '1',
    Transport_model: '1',
    Transport_number: '1'
  };

class Ticket extends Component {
  static methodAreOk() {
    return true;
  }
  render() {
    console.log(this.props);
    return (
      <div className="container">
        <div className="block-ticket">
          <div className="block-ticket-table-border" />
          <div className="block-ticket-table">
            <table>
              <tr id="block-ticket-table-header">
                <th><p><span>ID</span> {this.props.params.hash}</p></th><th><p><span>Направление:</span> {TicketData.TicketWay}</p></th><th><p><span>Дата</span> {TicketData.TicketDate}</p></th>
              </tr>
            </table>
            <table>
              <tr>
                <td id="name-cell">
                  <p className="block-ticket-table-left_col">ФИО:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.FullName}</p>
                </td>
                <td id="passengers-cell">
                  <p>Число пассажиров:</p>
                  <p className="block-ticket-table-info">{TicketData.Passengers}</p>
                </td>
              </tr>
              <tr>
                <td id="mail-cell">
                  <p className="block-ticket-table-left_col">Электронный адрес:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.Email}</p>
                </td>
                <td id="passenger1_name-cell">
                  <p>ФИО:</p>
                  <p className="block-ticket-table-info">{TicketData.Passenger1_name}</p>
                </td>
              </tr>
              <tr>
                <td id="passport-cell">
                  <p className="block-ticket-table-left_col">Паспортные данные:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.Passport}</p>
                </td>
                <td id="passenger1_passport-cell">
                  <p>Паспортные данные:</p>
                  <p className="block-ticket-table-info">{TicketData.Passenger1_passport}</p>
                </td>
              </tr>
              <tr>
                <td id="transport_type-cell">
                  <p className="block-ticket-table-left_col">Тип транспорта:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">
                    <span id="transport_type-auto" className={`transport_type-left ${TicketData.Transport_type === 0 ? 'transport_type-selected' : ''}`}>Автомобиль</span>
                    <span id="transport_type-buses" className={`transport_type-right ${TicketData.Transport_type === 1 ? 'transport_type-selected' : ''}`}>Спецтехника</span>
                  </p>
                </td>
                <td id="passenger2_name-cell">
                  <p>ФИО:</p>
                  <p className="block-ticket-table-info">{TicketData.Passenger2_name}</p>
                </td>
              </tr>
              <tr>
                <td id="transport_model-cell">
                  <p className="block-ticket-table-left_col">Марка и модель автомобиля:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.Transport_mark}</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.Transport_model}</p>
                </td>
                <td id="passenger2_passport-cell">
                  <p>Паспортные данные:</p>
                  <p className="block-ticket-table-info">{TicketData.Passenger2_passport}</p>
                </td>
              </tr>
              <tr>
                <td id="transport_data-cell">
                  <p className="block-ticket-table-left_col">Гос. номер транспотроно средства:</p>
                  <p className="block-ticket-table-left_col block-ticket-table-info">{TicketData.Transport_number}</p>
                </td>
                <td>
                  <p><a href="" title="Отмена"><img src="/images/1482881048_arrow-back.png" alt="" title="Отмена" /></a> &nbsp; &nbsp; &nbsp; Вернуть билет</p>
                  <p><a href="" title="Печать"><img src="/images/1483052167_vector_66_15.png" alt="" title="Печать" /></a> &nbsp; &nbsp; &nbsp; Скачать и распечатать</p>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Ticket;
