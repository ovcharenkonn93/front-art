import React, { Component, PropTypes } from 'react';
import Select from 'react-select';
import { isNull } from 'lodash';

const options = [
  { value: 'Toyota', label: 'Toyota' },
  { value: 'Nisan', label: 'Nisan' }
];

class SelectAutoModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auto: '',
      searchable: true,
      selectValue: null,
      clearable: true
    };
  }

  updateValue = (newValue) => {
    this.props.onSelect(newValue);
  }


  render() {
    const carModelSelected = this.props.carModelSelected;
    if (isNull(carModelSelected)) {
      return null;
    }
    const { carModel } = this.props;
    if (!carModel) {
      return null;
    }


    return (
      <div>
        <Select
          options={carModel}
          valueKey="id"
          labelKey="auto_model"
          simpleValue clearable={this.state.clearable}
          name="selected-auto" disabled={this.state.disabled}
          value={this.props.value} onChange={this.updateValue}
          searchable={this.state.searchable}
          placeholder="Выберите модель автомобиля:"
        />
      </div>


    );

  }
}


SelectAutoModel.propTypes = { carModel: PropTypes.array, carModelSelected: PropTypes.number };

export default SelectAutoModel ;
