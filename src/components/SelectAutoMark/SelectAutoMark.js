import React, { Component, PropTypes } from 'react';
import Select from 'react-select';
import { isNull } from 'lodash';

// const options = [
//   { id: 1, value: 'Almera', label: 'Almera' },
//   { id: 2, value: 'Camry', label: 'Camry' }
// ];

/*
const options = {
  Nisan:
  {
    { value: 'Almera', label: 'Almera' }
  },
  Toyota:
  {

  },
  VolksWagen:
  {

  },
  Lada:
  {

  }
}; */


class SelectAutoMark extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auto: '',
      searchable: true,
      selectValue: null,
      clearable: true
    };
  }

  updateValue = (newValue) => {
   // console.log('sasd');
    this.props.onSelect(newValue);

  }


  render() {


    const { carMake, value } = this.props;
    const marginForSelect = !isNull(this.state.selectValue) ? 'margin_bottom_20' : '';
    return (
      <div className={marginForSelect} >
        <Select
          options={carMake}
          valueKey="id"
          labelKey="auto_marka"
          simpleValue clearable={this.state.clearable}
          name="selected-auto" disabled={this.state.disabled}
          value={value} onChange={this.updateValue}
          searchable={this.state.searchable} placeholder="Выберите марку автомобиля:"
        />
      </div>


    );

  }
}


SelectAutoMark.propTypes = { carMake: PropTypes.array };

export default SelectAutoMark ;
