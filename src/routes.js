import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import Layout from './containers/Layout/Layout';
import NotFound from './components/NotFound/NotFound';
import BrowserStorage from './common/browserStorage';
import IndexPage from './components/IndexPage/IndexPage';
import Personal from './containers/Personal';
import Ticket from './components/Ticket/Ticket';
import SuccessfulPurchase from './components/SuccessfulPurchase/SuccessfulPurchase';
import RejectPurchase from './components/SuccessfulPurchase/RejectPurchase';


export default (store) => {
  const requireLogin = (next, replace, cb) => {
  /* if (!BrowserStorage.get('token')) {
      replace({
        pathname: '/',
        state: { nextPathname: next.location.pathname }
      });
    }*/
    cb();
  };

  const logout = (next, replace, cb) => {
    BrowserStorage.remove('token');
    store.dispatch({ type: 'LOGOUT' });

    replace({
      pathname: '/',
      state: { nextPathname: next.location.pathname }
    });
    cb();
  };
  return (
    <Route>
      <Route path="/">
        <Route component={Layout}>
          <IndexRoute component={IndexPage} />
          <Route path="logout" onEnter={logout} />
          <Route path="success" component={SuccessfulPurchase} />
          <Route path="fail" component={RejectPurchase} />
        </Route>
        <Route component={Layout}>
          <Route path="personal" onEnter={requireLogin}>
            <IndexRedirect to="page" />
            <Route path="page" component={Personal} />
            <Route path="ticket/:hash" component={Ticket} />
          </Route>
        </Route>
        <Route path="*" component={NotFound} status={404} />
      </Route>
    </Route>
  );
};
