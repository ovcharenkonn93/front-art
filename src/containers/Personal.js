import React, { Component, PropTypes } from 'react';
import { connect, Link } from 'react-redux';
import Modal from 'react-modal';
import TicketItem from '../components/TicketItem/TicketItem';


const mapStateToProps = state => ({
  layout: state.layout
});


const Tickets = [
  {
    TicketID: '3457645',
    TicketWay: 'Приобье-Салехард,Салехард-Приобье',
    TicketDate: '16.12.16'
  },
  {
    TicketID: '3457645',
    TicketWay: 'Приобье-Салехард,Салехард-Приобье',
    TicketDate: '16.12.16'
  }
];


@connect(mapStateToProps)
class Personal extends Component {
  static methodsAreOk() {
    return true;
  }

  render() {


    if (!Tickets.length) {
      return null;
    }

    return (
      <div className="container">
        <div className="block-personal_panel">
          <div className="block-personal_panel-table-border" />
          <div className="block-personal_panel-table">
            <table>
              <thead>
                <tr id="block-personal_panel-table-header">
                  <th><p>ID</p></th><th><p>Направления</p></th><th><p>Дата</p></th><th><p>Действия</p></th>
                </tr>
              </thead>
              <tbody>
              {
                            Tickets.map((item, index) => (
                              <TicketItem key={index} data={item} />
                  ))}
                  </tbody>
            </table>
          </div>
        </div>
      </div>


    );

  }
}


export default Personal;

