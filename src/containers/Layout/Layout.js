import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import ErrorSelector from '../../components/ErrorSelector/ErrorSelector';
import * as actions from './Layout.actionTypes';

const mapStateToProps = state => ({
  ...state.layout
});

@connect(mapStateToProps)
class Layout extends Component {
  static methodsAreOk() {
    return true;
  }

  closeModal = () => {
    this.props.dispatch({ type: actions.CLOSE_ERROR });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="mdl-layout__container">
          {this.props.children}
          <Footer />
        </div>
        {this.props.open && <ErrorSelector data={this.props.error_meesage} open={this.props.open} closeModal={this.closeModal} />}
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.object.isRequired
};

export default Layout;

