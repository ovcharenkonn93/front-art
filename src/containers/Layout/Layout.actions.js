import { SHOW_ERROR } from './Layout.actionTypes';

export function serverError(error) {
  return {
    type: SHOW_ERROR,
    error
  };
}
