import * as actions from './Layout.actionTypes';

const initialState = {
  open: false,
  error_meesage: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SHOW_ERROR:
      return {
        ...state,
        open: true,
        error_meesage: action.error
      };
    case actions.CLOSE_ERROR:
      return {
        ...state,
        open: false,
        error_meesage: ''
      };
    default: {
      return {
        ...state
      };
    }
  }
};
