import React, { Component, PropTypes } from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import NewsItem from '../components/NewsItem/NewsItem';


const mapStateToProps = state => ({
  ...state.layout
});


const myNews = [
  {
    newsData: '16 Декабря',
    newsTitle: 'Работа керченской переправы возобновлена',
    newsText: 'Осуществляется по фактическим договорным условиям',
    newsFullText: 'Осуществляется по фактическим договорным условиям'
  },
  {
    newsData: '17 Декабря',
    newsTitle: 'Работа керченской переправы возобновлена',
    newsText: 'Осуществляется по фактическим договорным условиям',
    newsFullText: 'Осуществляется по фактическим договорным условиям'
  },
  {
    newsData: '18 Декабря',
    newsTitle: 'Работа керченской переправы возобновлена',
    newsText: 'Осуществляется по фактическим договорным условиям',
    newsFullText: 'Осуществляется по фактическим договорным условиям'
  }
];


const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    right: '0px',
    bottom: '0px',
    border: 'none',
    background: 'transparent',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '0px',
    outline: 'none',
    padding: '0px'

  }

};


@connect(mapStateToProps)
class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      newsItemHeader: '',
      newsItemText: '',
      newsItemFullText: '',
      newsItemDate: ''

    };
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  }

  newsClick = (data, title, text, fullText) => {

  //  this.setState({ newsItemDate: item.newsData, newsItemHeader: item.newsTitle, newsItemText: item.NewsText });
    // this.setState({ modalIsOpen: true });
  }


  render() {


    if (!myNews.length) {
      return null;
    }

    return (
      <div>
        <h3 className="block-news__header">Новости</h3>
        <div className="block-news__elements">
          {
            myNews.map((item, index) => (
              <NewsItem key={index} data={item} onNewsItemClick={this.newsClick} />
        ))}

        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel=""
          style={style}
        >
          <div className="overlay">
            <div className="block-modal">
              { /* <a href="#close" onClick={this.closeModal} className="close-modal" /> */ }
              <span onClick={this.closeModal} className="close-modal" />
              <p className="modal-header">Заполните поля необходимой информацией</p>

              <div className="modal-form">
                <div className="modal-info-block">
                  <label htmlFor="full-name">
                  Введите ФИО:</label>
                  <input id="full-name" name="full-name" type="text" placeholder="Иванов Иван Петрович" />
                </div>

                <div className="modal-info-block">
                  <label htmlFor="e-mail">
                  Введите ваш электронный адрес: </label>
                  <input id="e-mail" name="e-mail" type="text" placeholder="pochta@mail.ru" />
                </div>

              </div>

              <div className="modal-push-btn">
                <a href="#" className="modal-lg-btn">Перейти к оплате</a>
              </div>


            </div>
          </div>
        </Modal>


      </div>


    );

  }
}


export default News;
