import React, { Component, PropTypes } from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import NewsItem from '../../components/NewsItem/NewsItem';
import * as actions from './News.actionTypes';

const mapStateToProps = state => ({
  ...state.news
});

const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    right: '0px',
    bottom: '0px',
    border: 'none',
    background: 'transparent',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '0px',
    outline: 'none',
    padding: '0px'

  }

};


@connect(mapStateToProps)
class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsItemHeader: '',
      newsItemText: '',
      newsItemFullText: '',
      newsItemDate: ''

    };
  }

  componentDidMount() {
    this.props.dispatch({
      type: actions.NEWS_INIT
    });
  }

  openModal = () => {
    this.props.dispatch({ type: actions.NEWS_MODAL_TRIGGER });
  }

  closeModal = () => {
    this.props.dispatch({ type: actions.NEWS_MODAL_TRIGGER });
  }

  newsClick = (data, title, text, fullText) => {
    this.setState({ newsItemHeader: title, newsItemDate: data, newsItemText: text, newsItemFullText: fullText, modalIsOpen: true });

  //  this.setState({ newsItemDate: item.newsData, newsItemHeader: item.newsTitle, newsItemText: item.NewsText });
    // this.setState({ modalIsOpen: true });
  }


  render() {
    const { news } = this.props;

    if (!news.length) {
      return null;
    }

    return (
      <div>
        <div className="content_center mdl-layout__content" >
          <div className="mdl-grid">
            <div className="mdl-cell mdl-cell--1-col mdl-cell--hide-tablet mdl-cell--hide-phone" />
            <div className="mdl-cell mdl-cell--10-col">
        <h3 className="block-news__header">Новости</h3>
        <div className="block-news__elements">
          {
            news.map((item, index) => (
              <NewsItem key={index} data={item} onNewsItemClick={this.newsClick} />
            ))}

        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel=""
          style={style}
        >
          <div className="overlay">
            <div className="block-modal">
              { /* <a href="#close" onClick={this.closeModal} className="close-modal" /> */ }
              <span onClick={this.closeModal} className="close-modal" />
              <p className="modal-header">Заполните поля необходимой информацией</p>

              <div className="modal-form">
                <div className="modal-info-block">
                  <label htmlFor="full-name">
                  Введите ФИО:</label>
                  <input id="full-name" name="full-name" type="text" placeholder="Иванов Иван Петрович" />
                </div>

                <div className="modal-info-block">
                  <label htmlFor="e-mail">
                  Введите ваш электронный адрес: </label>
                  <input id="e-mail" name="e-mail" type="text" placeholder="pochta@mail.ru" />
                </div>

              </div>

              <div className="modal-push-btn">
                <a href="#" className="modal-lg-btn">Перейти к оплате</a>
              </div>


            </div>
          </div>
        </Modal>
            </div>
          </div>
        </div>
      </div>


    );

  }
}


export default News;
