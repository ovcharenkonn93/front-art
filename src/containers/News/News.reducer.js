import * as actions from './News.actionTypes';

const initialState = {
  loading: true,
  newsModalIsOpen: false,
  news: []
};


export default (state = initialState, action) => {
  switch (action.type) {
    case actions.NEWS_INIT:
      return {
        ...state
      };
    case actions.NEWS_INITIATED:
      return {
        ...state,
        loading: false,
        news: action.results
      };
    case actions.NEWS_MODAL_TRIGGER:
      return {
        ...state,
        newsModalIsOpen: !state.newsModalIsOpen
      };
    default:
      return {
        ...state
      };
  }
};
