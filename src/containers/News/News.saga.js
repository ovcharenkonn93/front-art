import { put, call, fork } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import api from '../../Api';
import * as actions from './News.actionTypes';
import { serverError } from '../../containers/Layout/Layout.actions';

function* getPassword() {
  yield* takeEvery(actions.NEWS_INIT, function* load() {
    try {
      const data = yield call(api.news.getNews);
      yield put({ type: actions.NEWS_INITIATED, ...data });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.NEWS_INIT_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

export default function* watch() {
  yield [
    fork(getPassword)
  ];
}
