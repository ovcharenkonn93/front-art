import { put, call, fork } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import api from '../../Api';
import * as actions from './SelectFromToList.actionTypes';
import { serverError } from '../../containers/Layout/Layout.actions';

function* init() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_INIT, function* load() {
    try {
      const data = yield call(api.selectFromToList.getRouteFrom);
      yield put({ type: actions.SELECT_FROM_TO_LIST_INITIATED, ...data });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

function* getForm() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_GET_FORM, function* load(action) {
    try {
      const { dateTo, dateFrom, auto_model, auto_number, first_name, document, email, user, amount, passengers } = action;
      const data = yield call(api.selectFromToList.getBuyId, { dateTo, dateFrom, auto_model, auto_number, first_name, document, email, user, amount, passengers });
      yield put({ type: actions.SELECT_FROM_TO_LIST_GET_FORM_COMPLETE, data });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}


function* getTo() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_GET_PAROM_FROM, function* load(action) {
    try {
      const data = yield call(api.selectFromToList.getTo, action.item);
      yield put({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_FROM_COMPLETE, ...data });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

function* getDates() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_GET_PAROM_TO, function* load(action) {
    try {
      let data = [];
      if (action.double) {
        data = yield call(api.selectFromToList.getDatesDouble, action.from, action.to);
      }
      const toRoute = yield call(api.selectFromToList.getDates, action.from, action.to);

      yield put({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_TO_COMPLETE, data, toRoute });

    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

function* registerAuto() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_REGISTER_AUTO_SEND, function* register(action) {
    try {
      yield call(api.selectFromToList.registerAuto, action.autoMarka, action.autoModel);
      yield put({ type: actions.SELECT_FROM_TO_LIST_REGISTER_AUTO_SEND_COMPLETE });

    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });

    }
  });
}

function* getMarks() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_GET_MARKS, function* getCB() {
    try {
      const results = yield call(api.selectFromToList.getModels);
      yield put({ type: actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES_COMPLETE, ...results });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  }
  );
}

function* getCatOrBuses() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES, function* getCB() {
    try {
      const results = yield call(api.selectFromToList.getModels);
      yield put({ type: actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES_COMPLETE, ...results });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  }
  );
}

function* getAutoModels() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_GET_AUTO_MODELS, function* getCB(action) {
    try {
      const results = yield call(api.selectFromToList.getCarOrBuses, action.car, action.buses, action.auto_marka);
      yield put({ type: actions.SELECT_FROM_TO_LIST_GET_AUTO_MODELS_COMPLETE, ...results });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });

    }
  });
}

function* getPrice() {
  yield* takeEvery(actions.SELECT_FROM_TO_LIST_SELECT_MODEL, function* getCB(action) {
    try {
      const results = yield call(api.selectFromToList.getPrice, action.selectCarModel,
        action.point_from, action.point_to, action.double);
      yield put({ type: actions.SELECT_FROM_TO_LIST_SELECT_MODEL_COMPLETE, ...results });
    } catch (error) {
      yield put(serverError('Произошла ошибка, повторите позже'));
      yield put({ type: actions.SELECT_FROM_TO_LIST_ERROR, error: 'Произошла ошибка, повторите позже' });
    }
  });
}

export default function* watch() {
  yield [
    fork(init),
    fork(getTo),
    fork(registerAuto),
    fork(getDates),
    fork(getCatOrBuses),
    fork(getMarks),
    fork(getAutoModels),
    fork(getPrice),
    fork(getForm)
  ];
}
