import { findIndex, remove, set, find } from 'lodash';
import * as actions from './SelectFromToList.actionTypes';

const initialState = {
  swichDirections: true,
  orderBtn: false,
  modalIsOpen: false,
  modalForAddAuto: false,
  pointFrom: null,
  pointTo: null,
  dateFrom: null,
  dateTo: null,
  date: false,
  phone: '+7',
  email: '',
  modelAdd: '',
  markaAdd: '',
  govNumber: '',
  fullName: '',
  form: null,
  selectCar: true,
  selectBuses: false,
  selectCarModel: null,
  selectCarMark: null,
  passengerCount: 0,
  carModels: [],
  carMarks: [],
  pointsFrom: [],
  pointsTo: [],
  datesFrom: [],
  firstPriceName: null,
  firstPriceCount: null,
  twoPriceName: null,
  twoPriceCount: null,
  passport: '',
  userDataForTicket: {
    customer: {
      fullName: '',
      email: '',
      passportCode: '',
      typeAuto: false,
      carMake: '',
      carModel: '',
      gosNum: ''
    },
    passengers: [
    ]
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SELECT_FROM_TO_LIST_INIT:
      return {
        ...state
      };
    case actions.SELECT_FROM_TO_LIST_INITIATED:
      return {
        ...state,
        pointsFrom: action.results
      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_TO:
      return {
        ...state,
        pointTo: action.to
      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_FROM:
      return {
        ...state,
        pointFrom: action.item,
        pointsTo: [],
        pointTo: null,
        date: null,
        datesFrom: [],
        dateFrom: null,
        dateTo: null,
        datesTo: []
      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_FROM_COMPLETE:
      return {
        ...state,
        pointsTo: action.results,
        date: null

      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_TO_COMPLETE:
      return {
        ...state,
        datesFrom: action.toRoute.results,
        datesTo: action.data.results,
        dateTo: null
      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE:
      return {
        ...state

      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE_COMPLETE:
      return {
        ...state,
        dateFrom: action.item,
        pointFromDate: find(state.datesFrom, o => (o.parom_date === action.item.format('YYYY-MM-DD'))).id

      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE_BACK:
      return {
        ...state

      };
    case actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE_BACK_COMPLETE:
      return {
        ...state,
        dateTo: action.item,
        pointToDate: find(state.datesTo, o => (o.parom_date === action.item.format('YYYY-MM-DD'))).id
      };
    case actions.SELECT_FROM_TO_LIST_ERROR: {
      return {
        ...state
      };
    }
    case actions.SELECT_FROM_TO_LIST_SELECT_MODEL:
      return {
        ...state,
        selectCarModel: action.selectCarModel
      };
    case actions.SELECT_FROM_MODAL_TRIGGER:
      return {
        ...state,
        modalIsOpen: !state.modalIsOpen
      };
    case actions.SELECT_FROM_TO_LIST_SWITCH_DIRECTION:
      return {
        ...state,
        dateTo: null,
        swichDirections: !state.swichDirections,
        firstPriceName: null,
        twoPriceName: null,
        selectCarMark: null,
        selectCarModel: null,
      };

    case actions.SELECT_FROM_TO_LIST_SELECT_MARK:
      return {
        ...state,
        selectCarMark: action.selectCarMark
      };

    case actions.SELECT_FROM_TO_LIST_DELETE_USER: {
      const clonedPassanger = [...state.userDataForTicket.passengers];
      remove(clonedPassanger, o => o.id == action.item);
      /* eslint-disable */
      console.log(clonedPassanger);

      clonedPassanger.map((passanger, index) => {
        clonedPassanger[index] = { ...passanger, id: index + 1 };
      });
      /* eslint-enable */
      return {
        ...state,
        userDataForTicket: { passengers: clonedPassanger }
      };
    }
    case actions.SELECT_FROM_TO_LIST_ADD_USER: {
      const clonedPassanger = [...state.userDataForTicket.passengers, {
        id: state.userDataForTicket.passengers.length + 1,
        pasFullName: '',
        pasPassportCode: ''
      }];
      return {
        ...state,
        userDataForTicket: { passengers: clonedPassanger }
      };
    }
    case actions.SELECT_FROM_TO_LIST_PASSPORT_CHANGE_PASSAGER: {
      const clonedPassanger = [...state.userDataForTicket.passengers];
      const index = findIndex(clonedPassanger, o => o.id == action.userId);
      clonedPassanger[index].pasPassportCode = action.value;
      return {
        ...state,
        userDataForTicket: { passengers: clonedPassanger }
      };
    }
    case actions.SELECT_FROM_TO_LIST_NAME_CHANGE_PASSAGER: {
      const clonedPassanger = [...state.userDataForTicket.passengers];
      const index = findIndex(clonedPassanger, o => o.id == action.userId);
      clonedPassanger[index].pasFullName = action.value;
      return {
        ...state,
        userDataForTicket: { passengers: clonedPassanger }
      };
    }
    case actions.SELECT_FROM_TO_LIST_CLOSE_AUTO_ADD:
      return {
        ...state,
        modalIsOpen: true,
        modalForAddAuto: false
      };
    case actions.SELECT_FROM_TO_LIST_OPEN_AUTO_ADD:
      return {
        ...state,
        modalIsOpen: false,
        modalForAddAuto: true,
        messageAutoAdd: false
      };
    case actions.SELECT_FROM_TO_LIST_REGISTER_AUTO_SEND_COMPLETE:
      return {
        ...state,
        messageAutoAdd: true
      };
    case actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES:
      return {
        ...state,
        carMarks: [],
        carModels: [],
        selectCarMark: null,
        selectCarModel: null,
        selectBuses: action.buses,
        selectCar: action.car
      };
    case actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES_COMPLETE:
      return {
        ...state,
        carMarks: action.results
      };
    case actions.SELECT_FROM_TO_LIST_GET_AUTO_MODELS_COMPLETE:
      return {
        ...state,
        carModels: action.results
      };

    case actions.SELECT_FROM_TO_LIST_FIELD: {
      const clonedState = { ...state };
      set(clonedState, action.field, action.value);
      return {
        ...state,
        ...clonedState
      };
    }
    case actions.SELECT_FROM_TO_LIST_SELECT_MODEL_COMPLETE: {
      const { two } = action.results;
      console.log(two);
      return {
        ...state,
        firstPriceName: action.results.first.name,
        firstPriceCount: action.results.first.price,
        twoPriceName: two ? two.name : null,
        twoPriceCount: two ? two.price : null
      };
    }
    case actions.SELECT_FROM_TO_LIST_GET_FORM_COMPLETE: {
      return {
        ...state,
        dev: action.data
      };
    }
    default:
      return {
        ...state
      };
  }
};
