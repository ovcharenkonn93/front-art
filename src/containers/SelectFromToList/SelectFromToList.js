import React, { Component, PropTypes } from 'react';
import { findIndex, remove, isNull, find } from 'lodash';
import * as EmailValidator from 'email-validator';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import InputElement from 'react-input-mask';
import SelectFrom from '../../components/SelectFrom/SelectFrom';
import SelectDirection from '../../components/SelectDirection/SelectDirection';
import SelectTo from '../../components/SelectTo/SelectTo';
import SelectDate from '../../components/SelectDate/SelectDate';
import SelectAutoMark from '../../components/SelectAutoMark/SelectAutoMark';
import SelectAutoModel from '../../components/SelectAutoModel/SelectAutoModel';
import * as actions from './SelectFromToList.actionTypes';

const mapStateToProps = state => ({
  ...state.selectFromToList
});

const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    position: 'absolute',
    top: '0px',
    left: '0px',
    right: '0px',
    bottom: '0px',
    border: 'none',
    background: 'transparent',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '0px',
    outline: 'none',
    padding: '0px'

  }

};


const options = [
  { value: 1, label: 'Toyota' },
  { value: 2, label: 'Nisan' }
];

const options2 = [
  { value: 'Toyota1', label: 'Toyota1' },
  { value: 'Nisan1', label: 'Nisan1' }
];

@connect(mapStateToProps)
class SelectFromToList extends Component {


  componentDidMount() {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_INIT });

  }

  selectFrom = (item) => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_FROM, item });
  };

  changePassangerName = (event, userId) => {
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_NAME_CHANGE_PASSAGER,
      userId,
      value: event.target.value
    });
    this.agree.checked = false;
  };


  checkAggree = () => {
    const { fullName, email, govNumber, phone, datesFrom, datesTo, selectCarModel, passport, dateTo, dateFrom,
      userDataForTicket: { passengers } } = this.props;

    const datefrom = find(datesFrom, o => (o.parom_date === dateFrom.format('YYYY-MM-DD')));
    let dateto = null;
    if (dateTo) {
      dateto = find(datesTo, o => (o.parom_date === dateFrom.format('YYYY-MM-DD')));
    }
    let amount = 0;
    if (this.props.firstPriceName && this.props.twoPriceName) {
      amount = this.props.firstPriceCount + this.props.twoPriceCount - (
        (this.props.firstPriceCount + this.props.twoPriceCount) * 0.05);
    } else {
      amount = this.props.firstPriceCount + this.props.twoPriceCount;
    }

    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_FORM,
      dateTo: dateto ? dateto.id : null,
      dateFrom: datefrom.id,
      auto_model: selectCarModel,
      auto_number: govNumber,
      first_name: fullName,
      document: passport,
      email,
      user: phone,
      amount,
      passengers
    });
  };


  checkFileds = () => {
    const { fullName, email, govNumber, phone, datesFrom, datesTo, selectCarModel, passport, dateTo, dateFrom, swichDirections, firstPriceCount, twoPriceCount } = this.props;
    if (!fullName || !EmailValidator.validate(email) || !govNumber || phone.length !== 12 || selectCarModel === null || !passport || (!swichDirections && dateTo === null) || dateFrom === null) {
      return;
    }
    if (this.agree.checked) {
      this.checkAggree();
    }

  };

  changeField = (event, field) => {
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_FIELD,
      value: event.target.value,
      field
    });
    if (this.agree.checked) {
      const target = event.target;
      if (target.type !== 'checkbox') {
        this.agree.checked = false;
      } else {
        this.checkFileds();

      }
    }

  };

  changePassangerPassport = (event, userId) => {
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_PASSPORT_CHANGE_PASSAGER,
      userId,
      value: event.target.value
    });
    this.agree.checked = false;

  };


  selectTo = (item) => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_TO, double: !this.props.swichDirections, from: this.props.pointFrom, to: item });
  };

  selectStartDate = (item) => {
    console.log(item);
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE_COMPLETE, item });
  };

  selectArrivalDate = (item) => {
    console.log(item);
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_DATE_BACK_COMPLETE, item });
  };

  selectAutoFromList = (val) => {
  };

  openModal = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_MODAL_TRIGGER });
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_MARKS });
  };

  closeModal = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_MODAL_TRIGGER });
  };


  swichDirection = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_SWITCH_DIRECTION });
    const { swichDirections, pointFrom, pointTo } = this.props;
    if (pointFrom && pointTo) {
      this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_TO, double: swichDirections, from: pointFrom, to: pointTo });
    }
  };

  changeDirection = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_SWITCH_DIRECTION });
    const { swichDirections, pointFrom, pointTo } = this.props;
    if (pointFrom && pointTo) {
      this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_PAROM_TO, double: swichDirections, from: pointFrom, to: pointTo });
    }
  };

  deletePassanger = (item) => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_DELETE_USER, item });
  };

  addPassanger =() => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_ADD_USER });
  };

  AutoOrBuses = () => {
    const { selectCar, selectBuses } = this.props;
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_AUTO_OR_BUSES,
      car: !selectCar,
      buses: !selectBuses
    });
  };


  selectMark = (id) => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_SELECT_MARK, selectCarMark: id });
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_GET_AUTO_MODELS,
      car: this.props.selectCar,
      buses: this.props.selectBuses,
      auto_marka: id
    });
  };

  selectModel = (id) => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_SELECT_MODEL, selectCarModel: id, point_from: this.props.pointFrom, point_to: this.props.pointTo, double: !this.props.swichDirections });
  };

  openModalAddAutos = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_OPEN_AUTO_ADD });
  };

  closeModalAddAuto = () => {
    this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_CLOSE_AUTO_ADD });

  };
  goPay =() => {
    const { dev } = this.props;
    // const { fullName,email,govNumber,phone,datesFrom,datesTo, selectCarModel,passport, dateTo, dateFrom } = this.props;
    // const datefrom = find(datesFrom, o => (o.parom_date === dateFrom.format('YYYY-MM-DD')));
    // let dateto = null;
    // if (dateTo) {
    //   dateto = find(datesTo, o => (o.parom_date === dateFrom.format('YYYY-MM-DD')));
    // }
    // this.props.dispatch({ type: actions.SELECT_FROM_TO_LIST_GET_FORM,
    //   dateTo: dateto ? dateto.id : null,
    //   dateFrom: datefrom.id,
    //   auto_model: selectCarModel,
    //   auto_number: govNumber,
    //   first_name: fullName,
    //   document: passport,
    //   email,
    //   user: phone });
    if (dev) this.form.submit();
  };

  showBuyBtn = () => {
    const { fullName, email, govNumber, phone, datesFrom, datesTo, selectCarModel, passport, dateTo, dateFrom, swichDirections } = this.props;

    if (!fullName || !EmailValidator.validate(email) || !govNumber || phone.length !== 12 || selectCarModel === null || !passport || (!swichDirections && dateTo === null) || dateFrom === null) {
      return null;
    }
    if (this.agree.checked) {
      return (
        <div className="modal-push-btn">
          <button onClick={this.goPay} className="modal-lg-btn">Перейти к оплате</button>
        </div>
      );
    }
    return null;
  };


  registrationAuto = () => {
    this.props.dispatch({
      type: actions.SELECT_FROM_TO_LIST_REGISTER_AUTO_SEND,
      autoMarka: this.props.markaAdd,
      autoModel: this.props.modelAdd
    });
  };

  render() {
    const { pointFrom, pointTo, dateFrom, dev, dateTo, swichDirections } = this.props;
    const switchClass = this.props.swichDirections ? 'switch disabledToggle' : 'switch enabledToggle';
    const { userDataForTicket: { passengers } } = this.props;
    const checkAuto = this.props.selectCar ? 'selectCar-btn-active' : 'selectCar-btn-default';
    const checkBuses = this.props.selectBuses ? 'selectBuses-btn-active' : 'selectBuses-btn-default';
    // const showSelectAutoModel = !isNull(this.state.selectCarModel)
    let activeBtn = !(isNull(pointFrom) || isNull(pointTo) || isNull(dateFrom));
    if (activeBtn && !swichDirections) {
      activeBtn = !(isNull(dateTo));
    }
    // if (dev) this.form.submit();
    const selectBtn = activeBtn ? 'select_from_to-btn' : 'select_from_to-btn disabled_block';
    return (

      <div className="container block-main_search">
        <div className="block-main_search-header">
          <h1>Поиск билетов</h1>
          <h2>Лучший способ купить билеты на паром</h2>
        </div>
        <div className="block-main_search-form" >
          <div>
            <SelectFrom data={this.props.pointsFrom} onSelectFrom={this.selectFrom} />
            <SelectDirection
              data={this.props.swichDirections}
              onSwichDirections={this.changeDirection}
            />
            <SelectTo
              data={this.props.pointsTo}
              onSelectTo={this.selectTo}
              active={!isNull(this.props.pointFrom)}
            />
            <SelectDate
              data={this.props.swichDirections}
              datesFrom={this.props.datesFrom}
              datesTo={this.props.datesTo}
              dateFrom={this.props.dateFrom}
              dateTo={this.props.dateTo}
              dubleAction={this.props.swichDirections}
              onSelectStartDate={this.selectStartDate}
              onSelectArrivalDate={this.selectArrivalDate}
            />
          </div>
        </div>
        <div className="row_right">
          <div className="block-main_search-form-swich">
            <div className="block-main_search-form-swich-text">
              <p>Заказать билет в обе стороны</p>
              { /* <p>Переправа в одну сторону</p>*/ }
            </div>
            <div className="block-main_search-form-swich-radio">
              <span className="toggle-bg" onClick={() => this.swichDirection()} >
                <span className={switchClass} />
              </span>
            </div>
          </div>
        </div>
        <div className="row_center">
          <button
            className={selectBtn}
            onClick={activeBtn ? () => this.openModal() : null}
          >Купить билет </button>
        </div>
        <Modal
          isOpen={this.props.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel=""
          style={style}
        >
          <div className="overlay">
            <div className="block-modal">
              { /* <a href="#close" onClick={this.closeModal} className="close-modal" /> */ }
              <span onClick={this.closeModal} className="close-modal" />
              <p className="modal-header">Заполните поля необходимой информацией</p>

              <div className="modal-form">
                <div className="modal-info-block">
                  <label htmlFor="full-name">
                  Введите ФИО:</label>
                  <input id="full-name" onChange={e => this.changeField(e, 'fullName')} value={this.props.fullName} name="full-name" type="text" placeholder="Иванов Иван Петрович" />
                </div>

                <div className="modal-info-block">
                  <label htmlFor="phone">
                  Введите ваш телефон: </label>
                  <InputElement id="phone" type="text" onChange={e => this.changeField(e, 'phone')} alwaysShowMask value={this.props.phone} mask="+79999999999" />
                </div>
                <div className="modal-info-block">
                  <label htmlFor="e-mail">
                  Введите ваш электронный адрес: </label>
                  <input id="e-mail" name="e-mail" value={this.props.email} type="text" onChange={e => this.changeField(e, 'email')} placeholder="pochta@mail.ru" />
                </div>

                <div className="modal-info-block">
                  <label htmlFor="passport">
                  Введите ваши паспортные данные:</label>
                  <input id="passport" name="passport" value={this.props.passport} onChange={e => this.changeField(e, 'passport')} type="text" placeholder="9999 99999999" />
                </div>

                <div className="modal-info-block">
                  <p className="modal-info-text">Выберите тип транспорта:</p>
                  <div className="modal-button-container">
                    <span className={checkAuto} onClick={() => this.AutoOrBuses()}> Автомобиль</span>
                    <span className={checkBuses} onClick={() => this.AutoOrBuses()}> Спецтехника</span>
                  </div>
                </div>

                <div className="modal-info-block">
                  <p className="modal-info-text">Укажите марку и модель автомобиля:</p>
                  <div className="modal-input-container">
                    <SelectAutoMark onSelect={this.selectMark} value={this.props.selectCarMark} carMake={this.props.carMarks} />
                    { !isNull(this.props.selectCarMark) ? <SelectAutoModel value={this.props.selectCarModel} onSelect={this.selectModel} carModel={this.props.carModels} /> : '' }
                  </div>
                  <span className="modal-del-passenger margin_top_10" onClick={this.openModalAddAutos}>( <span className="modal-del-passenger-text">Добавить автомобиль</span> )</span>
                </div>

                <div className="modal-info-block">
                  <label htmlFor="gov-number">Укажите государственный номер: </label>
                  <input id="gov-number" type="text" value={this.props.govNumber} onChange={e => this.changeField(e, 'govNumber')} placeholder="Введите гос. номер" />
                </div>

                <div className="modal-info-block">
                  <p className="modal-info-text"> Укажите число пассажиров: </p>
                  <div className="modal-info-passenger-control">
                    <span className="modal-info-passenger-counter" > {this.props.userDataForTicket.passengers.length} </span>
                    <span className="modal-info-add-passenger" onClick={() => this.addPassanger()} >Добавить пассажира</span>
                  </div>
                </div>
                {passengers.map((item, key) =>
                  <div className="modal-info-block" key={key}>
                    <div className="modal-info-header-block" >
                      <p className="modal-passenger-header">Пассажир {key + 1}</p>
                      <span className="modal-del-passenger" onClick={() => this.deletePassanger(item.id)}>( <span className="modal-del-passenger-text">Удалить пассажира</span> )</span>
                    </div>
                    <div className="modal-input-container">
                      <input name="car-make" type="text" placeholder="Введите ФИО пассажира:" onChange={e => this.changePassangerName(e, item.id)} value={item.pasFullName} />
                      <input name="car-model" type="text" placeholder="Введите паспортные данные:" value={item.pasPassportCode} onChange={e => this.changePassangerPassport(e, item.id)} />
                    </div>
                  </div>
                )}
                <div className="modal-info-block">
                  <p className="modal-info-text">Согласие на обработку персональных данных:</p>
                  <div className="modal-small-input">
                    <div className="custom-checkbox">
                      <input onChange={e => this.changeField(e, 'agree')} type="checkbox" id="agree_btn" ref={(agree) => { this.agree = agree; }} name="i_am_agree" />
                      <label htmlFor="agree_btn" />
                    </div>
                  </div>
                </div>
                <div className="modal-info-block">
                  {this.props.firstPriceName && <p className="modal-info-text">{this.props.firstPriceName}: <span style={{ fontWeight: 'bold' }}>{this.props.firstPriceCount} рублей</span></p>}
                  {this.props.twoPriceName && <p className="modal-info-text">{this.props.twoPriceName}: <span style={{ fontWeight: 'bold' }}>{this.props.twoPriceCount} рублей</span></p>}
                  {!swichDirections && <p className="modal-info-text">Скидка за покупку в две стороны:<b>5%</b> </p>}
                  {this.props.firstPriceName && this.props.twoPriceName && <p className="modal-info-text">Итого к оплате: <span style={{ fontWeight: 'bold' }}>{this.props.firstPriceCount + this.props.twoPriceCount - ((this.props.firstPriceCount + this.props.twoPriceCount) * 0.05) } рублей</span></p>}
                  {this.props.firstPriceName && !this.props.twoPriceName && <p className="modal-info-text">Итого к оплате: <span style={{ fontWeight: 'bold' }}>{this.props.firstPriceCount + this.props.twoPriceCount} рублей</span></p>}
                </div>
              </div>

              {this.showBuyBtn()}

            </div>
          </div>
        </Modal>
        <Modal
          isOpen={this.props.modalForAddAuto}
          onRequestClose={this.closeModalAddAuto}
          contentLabel=""
          style={style}
        >
          <div className="overlay">
            <div className="block-modal">
              <span onClick={this.closeModalAddAuto} className="close-modal" />
              <p className="modal-header-add-auto">Заполните поля необходимой информацией</p>

              <div className="modal-form-add-auto">
                <div className="modal-info-block-add-auto">
                  <label htmlFor="full-name">
                  Название марки транспортного средства:</label>
                  <input value={this.props.markaAdd} onChange={e => this.changeField(e, 'markaAdd')} name="full-name" type="text" placeholder="Lada" />
                </div>

                <div className="modal-info-block-add-auto">
                  <label htmlFor="gov-number">Модель транспортного средства: </label>
                  <input value={this.props.modelAdd} onChange={e => this.changeField(e, 'modelAdd')} type="text" placeholder="Vesta" />
                </div>
                {this.props.messageAutoAdd && <p className="autoAdd">Авто отправлено на проверку!</p>}
                <div className="modal-push-btn">
                  <button onClick={this.registrationAuto} className="modal-middle-btn">Зарегистрировать</button>
                </div>

              </div>
            </div>


          </div>

        </Modal>
        <form action="https://demomoney.yandex.ru/eshop.xml" method="post" style={{ display: 'none' }} ref={(form) => { this.form = form; }} name="ShopForm" id="payment_form_id">
          <span dangerouslySetInnerHTML={{ __html: this.props.dev }} />
        </form>
      </div>


    );
  }
}


export default SelectFromToList;
