import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import header from '../components/Header/Header.reducer';
import news from '../containers/News/News.reducer';
import selectFromToList from '../containers/SelectFromToList/SelectFromToList.reducer';
import selectDate from '../components/SelectDate/SelectDate.reducer';
import layout from '../containers/Layout/Layout.reducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  header,
  news,
  selectFromToList,
  selectDate,
  layout,
});

export default rootReducer;
